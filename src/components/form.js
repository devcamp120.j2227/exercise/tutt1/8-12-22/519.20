import { useEffect, useState } from "react";

const Form = () => {
    const [firstName, setFirstname] = useState(localStorage.getItem("firstName") || "");
    const [lastName, setLastname] = useState(localStorage.getItem("lastName") || "");

    const onFirstNameChangeHandler = (event) => {
        let value = event.target.value;

        setFirstname(value);
    }

    const onLastNameChangeHandler = (event) => {
        let value = event.target.value;

        setLastname(value);
    }

    useEffect(() => {
        localStorage.setItem("lastName", lastName);
        localStorage.setItem("firstName", firstName);

        document.title = lastName + " " + firstName;

    }, [firstName, lastName]);

    return (
        <div>
            <div>
                <input value={firstName} onChange={onFirstNameChangeHandler}/>
            </div>
            <div>
                <input value={lastName} onChange={onLastNameChangeHandler}/>
            </div>
            <p>{lastName} {firstName}</p>
        </div>
    )
}

export default Form;